#!/usr/bin/env ruby
# -*- coding: utf-8 -*-

# Copyright (C) 2012 Alexander Sidorov <alx.sidorov@gmail.com>

# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
# files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
# modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
# is furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS 
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


require "nokogiri"
require "open-uri"
require "optparse"
require "fileutils"


class FlibustaCrawler

  def initialize(url, ft_order, out_dir, timeout, verbose)
    @genre_url     = url
    @base_url      = "http://" + url.match(/http:\/\/(.*?)\//)[1]
    @out_dir       = out_dir.gsub /\/$/, ""
    @books_pattern = "ol a"
    @timeout       = timeout
    @verbose       = verbose
    @ft_order      = ft_order.split("/").map {|type| "(#{type})"}
    @headers       = {
      "User-Agent" => "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.91 Safari/537.11"
    }
  end


  def fetch
    FileUtils.mkdir_p @out_dir unless File.exists? @out_dir
    load_downloaded
    fetch_books get_book_links
  end


  private

  def die_n_exit
    puts "flibusta is dead, try later"
    exit!
  end


  def get_doc(url, options={})
    doc   = nil
    tries = 0
    begin
      doc = Nokogiri::HTML(open(URI.escape(url), options))
    rescue 
      puts $!
      puts "attempt to open [#{url}] failed, trying to repeat in 3 sec"
      sleep 3
      tries += 1
      if tries < 5 # repeat 5 times
        retry
      else
        die_n_exit
      end
    end
    return doc
  end


  def load_downloaded
    @downloaded = Dir.entries(@out_dir).select {|x| x != "." && x != ".."}.sort
  end


  # collects link to books from a genre page
  def get_book_links
    puts "Fetching book list" if @verbose
    doc   = get_doc @genre_url, @headers
    links = doc.css(@books_pattern).find_all {|link| link["href"].include? '/b/'}
  end

  
  # fetches link to a file from a book page
  def get_book_url(links)
    for i in 0..@ft_order.size - 1 do
      ftype = @ft_order[i]
      dlinks = links.find_all {|link| link.content == ftype}
      if dlinks.size == 1
        return [ dlinks[0]["href"], 
                 lambda {|ftype| ftype == "fb2" ? "fb2.zip" : ftype }.call(ftype.gsub(/[\(\)]/, "")) ]
      end
      puts "for this page desired format was not found, trying to fetch another" if @verbose && i < @ft_order.size - 1
    end
  end


  def already_downloaded?(name)
    @downloaded.grep(/#{name}(.fb2.zip$)|(.epub$)|(.mobi$)/).size > 0
  end


  def fetch_books(links)
    for i in 0..links.size-1 do
      link = links[i]
      puts "\nFetching: #{link.content} [#{i+1}/#{links.size}]" if @verbose

      if already_downloaded? link.content
        puts "This book already downloaded" if @verbose
        next
      end

      book_page  = get_doc(@base_url + link["href"], @headers)

      # fb2 epub mobi
      book_url = get_book_url book_page.css("a")

      if book_url.empty?
        puts "Unable to find a link for downloading" if @verbose 
      else
        cmd = "curl -A '#{@headers["User-Agent"]}' -e #{@base_url + link['href']} -L -o '#{@out_dir}/#{link.content}.#{book_url[1]}' #{@base_url + book_url[0]}"
        puts cmd if @verbose
        system cmd 
        sleep @timeout
      end
    end
  end

end




# Parsing command-line options

options = {}

optparse = OptionParser.new do |opts|
  opts.banner = "Usage: crawler.rb -s source [options]"

  options[:source] = nil
  opts.on( "-s", "--source SOURCE", "Url to desired genre (full list is here: http://(proxy.)flibusta.net/g)" ) do |src|
    options[:source] = src
  end

  options[:verbose] = false
  opts.on( "-v", "--verbose", "Output more information" ) do
    options[:verbose] = true
  end

  options[:ft_order] = "fb2/epub/mobi"
  opts.on( "-fo", "--file-order FORDER", "The order of the file types that try to download if desired format not exists (fb2/epub/mobi by default)" ) do |fto|
    options[:ft_order] = fto
  end

  options[:out_dir] = "out"
  opts.on( "-o", "--out-directory DIR", "Directory where files will be saved (out by default)" ) do |out_dir|
    options[:out_dir] = out_dir
  end

  options[:timeout] = 3
  opts.on( "-t", "--timeout TIMEOUT", "Timeout in s between downloads (3s by default)" ) do |timeout|
    options[:timeout] = timeout
  end


  opts.on( "-h", "--help", "Display this screen" ) do
    puts opts
    exit!
  end

end


# Entry-point

optparse.parse!

if options[:source]
  crawler = FlibustaCrawler.new options[:source], options[:ft_order], options[:out_dir], options[:timeout], options[:verbose]
  crawler.fetch
else
  puts optparse.banner
end
